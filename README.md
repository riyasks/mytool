# mytool



## Steps to Run

1, Clone the repository<br/>
2, Open ubuntu terminal<br/>
3, Run : sh myscript.sh<br/>

This will create the debian package for the python sample tool inside the mypythontool folder and automatically install the tool in the global env to access anywhere in the terminal


## Steps used inside the bash

1,Install python 3 dependency<br/>
2,Removing mytool if it already installed<br/>
3,Move the python file (eg: compiled one) to the dist folder of DEBIAN to create the package<br/>
4,Setting permission using chmod command<br/>
5,Removing the .deb file if exist<br/>
6,Debian build to create new deb package for mytool<br/>
7,Setting permission<br/>
8,Automatically get initialized upon successful installation of the debian python tool package with param --init<br/>
9,After installation removing deb file from the folder
Note : this tool will be install as a global package (bin/env) so that it can be use anywhere in the terminal like<br/>

mytool --init<br/>

output : <br/>
STARTING MYTOOL....<br/>
Received Argument: --init<br/>




