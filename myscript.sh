
#!/bin/bash
Green='\033[1;32m'
Yellow='\033[1;33m'
NC='\033[0m' # No Color


sudo apt-get update
sudo apt-get install python3.6 -y

printf "Installing dpkg package manager....\n"
sudo apt-get install dpkg
printf "Removing existing installed mytool....\n"
sudo apt remove mytool -y
rm -f mypythontool/usr/bin/mytool
cp mypythontool/mytool.py mypythontool/usr/bin/mytool
printf "Setting Permission\n"
chmod u+rx mypythontool/usr/bin/mytool
rm -f mypythontool.deb
printf "Debian packaging initialising....\n"
sudo dpkg -b mypythontool
printf "${Green}Debian packaging done!${NC}\n"
printf "Installing mypythontool.deb(mytool).....\n"
sudo dpkg -i mypythontool.deb
printf "Setting Permission\n"
chmod u+rx /usr/bin/mytool
printf "${Green}mytool installed successfully!${NC}\n"
rm -f mypythontool.deb
printf "Removing mypythontool.deb...\n"
printf "Initializing mytool with parameter --init\n"
mytool --init
printf "${Green}mytool tested succesfully with parameter --init${NC}\n"

